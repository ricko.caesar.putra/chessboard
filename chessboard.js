const makeChessboard = () => {
  // membuat chessboard
let chessboard=[];

  for (let i=0;i<8;i++){
    chessboard[i]=[];
    for (let j=0;j<8;j++){
      chessboard[i][j]=[];
      if (i==1) {
        chessboard[i][j]= "P-black";
      } else if (i==6){
        chessboard[i][j]= "P-White";
      } else if (i>=2 && i<=5){
        chessboard[i][j]= "-";
      } else if ((i==0 && j==0)||(i==0 && j==7)){
        chessboard[i][j]=("B Black");
      } else if ((i==0 && j==1)||(i==0 && j==6)){
        chessboard[i][j]= "K Black";  
      } else if ((i==0 && j==2)||(i==0 && j==5)){
        chessboard[i][j]= "M Black";
      } else if ((i==0 && j==3)){
        chessboard[i][j]= "RT Black";  
      } else if ((i==0 && j==4)){
        chessboard[i][j]= "RJ Black";  
      } else if ((i==7 && j==0)||(i==7 && j==7)){
        chessboard[i][j]= "B White";
      } else if ((i==7 && j==1)||(i==7 && j==6)){
        chessboard[i][j]= "K White";
      } else if ((i==7 && j==2)||(i==7 && j==5)){
        chessboard[i][j]= "M White";
      } else if ((i==7 && j==3)){
        chessboard[i][j]= "RT White";  
      } else if ((i==7 && j==4)){
        chessboard[i][j]= "RJ White";   }
  }
  }
   return chessboard
};

const printBoard = x => {
  //print sesuai format
  for (let k=0;k<8;k++){
    console.log(x[k]);
  }

  //print dalam bentuk table
  console.table(x);
}

printBoard(makeChessboard())